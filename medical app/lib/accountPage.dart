import 'package:flutter/material.dart';
import 'package:medi/homep.dart';
import 'package:medi/loginpage.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class UserListPage extends StatefulWidget {
  const UserListPage({super.key});

  @override
  _UserListPageState createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  List<Map<String, dynamic>> _users = [];

  @override
  void initState() {
    super.initState();
    _fetchUsers();
  }

  Future<void> _fetchUsers() async {
    final Database database = await openDatabase(
      join(await getDatabasesPath(), 'todolist8.db'),
    );

    final List<Map<String, dynamic>> users = await database.query('users');

    setState(() {
      _users = users;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: const Color.fromARGB(255, 133, 206, 243),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => YourWidget()),
            );
          },
        ),
        title: Text(
          'Account',
          style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Color.fromARGB(255, 133, 206, 243),
          items: [
            BottomNavigationBarItem(
              backgroundColor: Color.fromARGB(255, 133, 206, 243),
              icon: GestureDetector(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (BuildContext context) => YourWidget()),
                  );
                },
                child: Icon(
                  Icons.home_outlined,
                  color: Colors.black,
                ),
              ),
              label: 'Home',
            ),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.alarm,
                  color: Colors.black,
                ),
                label: "Alarm"),
            BottomNavigationBarItem(
                icon: GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (BuildContext context) => UserListPage()),
                    );
                  },
                  child: const Icon(
                    Icons.account_circle_outlined,
                    color: Colors.black,
                  ),
                ),
                label: "Account"),
          ]),
      body: _users.isEmpty
          ? Center(child: CircularProgressIndicator())
          : ListView.builder(
              itemCount: _users.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Image.asset(
                        "asset/image/p6.jpg",
                        height: 250,
                        width: 250,
                      ),
                      Row(
                        children: [
                          const Text(
                            "UserName: ",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            _users[index]['userName'],
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.normal),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          const Text(
                            "Contact: ",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          Text(_users[index]['contact'],
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.normal)),
                        ],
                      ),
                      const SizedBox(
                        height: 200,
                      ),
                      ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: WidgetStatePropertyAll(
                          const Color.fromARGB(255, 133, 206, 243),
                        )),
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (BuildContext context) => Login()),
                          );
                        },
                        child: const Text("SIGN OUT",
                            style: TextStyle(
                                fontSize: 16,
                                letterSpacing: 2.2,
                                color: Colors.black)),
                      ),
                    ],
                  ),
                );
              },
            ),
    );
  }
}
