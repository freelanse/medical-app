// ignore_for_file: use_build_context_synchronously

import 'dart:io';
import 'package:medi/accountPage.dart';
import 'package:medi/add_alarm.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart%20';
import 'package:provider/provider.dart';
import 'database.dart';
import 'model_anm.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as path;
import 'provider.dart';
import 'dart:async';
import 'package:intl/intl.dart';

class YourWidget extends StatefulWidget {
  const YourWidget({super.key});

  @override
  _YourWidgetState createState() => _YourWidgetState();
}

class _YourWidgetState extends State<YourWidget> {
  List<MediModelClass> data = [];
  final TextEditingController nameController = TextEditingController();
  final TextEditingController medicineController = TextEditingController();
  final TextEditingController startDateController = TextEditingController();
  final TextEditingController endDateController = TextEditingController();

  Uint8List? animalImage$petId;
  File? selectedIMage;
  Uint8List? medicineImage;

  XFile? pickedFile;
  XFile? pickedFile1; // it takes images from camera or gallary

  List<Color> mycolor = [
    Colors.pink.shade100,
    Colors.blue.shade100,
    Colors.purple.shade100,
    Colors.green.shade100,
    Colors.yellow.shade100
  ];

  List<File?> _images = [];
  final ImagePicker _pickeranimal = ImagePicker();
  final List<List<File?>> _medicinecards = [];
  final ImagePicker _pickermedicine = ImagePicker();

  @override
  void initState() {
    context.read<alarmprovider>().Inituilize(context);
    super.initState();
    context.read<alarmprovider>().GetData();
    super.initState();
    _loadImages();
    _loadImagesmedicine();
    _getData();
  }

  Future<void> _loadImagesmedicine() async {
    final directory2 = await getApplicationDocumentsDirectory();
    final imageDir1 = Directory('${directory2.path}/images');
    if (imageDir1.existsSync()) {
      final cardDirectories =
          imageDir1.listSync().whereType<Directory>().toList();
      for (var cardDir in cardDirectories) {
        final imageFiles =
            cardDir.listSync().map((item) => File(item.path)).toList();
        setState(() {
          _medicinecards.add(imageFiles);
        });
      }
    }
  }

  Future<void> _deletemedineCard(int cardIndex) async {
    final card = _medicinecards[cardIndex];
    for (var image in card) {
      if (image != null) {
        await image.delete();
      }
    }
    final directory = await getApplicationDocumentsDirectory();
    final cardDir = Directory('${directory.path}/images/card_$cardIndex');
    if (cardDir.existsSync()) {
      cardDir.deleteSync(recursive: true);
    }
    setState(() {
      _medicinecards.removeAt(cardIndex);
    });
  }

  Future<void> savemedicineimage(int cardIndex) async {
    if (pickedFile1 != null) {
      final directory = await getApplicationDocumentsDirectory();
      final imageDir = Directory('${directory.path}/images/card_$cardIndex');
      if (!imageDir.existsSync()) {
        imageDir.createSync(recursive: true);
      }
      final timestamp = DateTime.now().millisecondsSinceEpoch.toString();
      final fileName =
          '${path.basenameWithoutExtension(pickedFile1!.path)}_$timestamp${path.extension(pickedFile1!.path)}';
      final savedImage =
          await File(pickedFile1!.path).copy('${imageDir.path}/$fileName');
      setState(() {
        _medicinecards[cardIndex].add(savedImage);
      });
    } else {
      setState(() {
        _addmedicineimageFromAssets(cardIndex);
      });
    }
    pickedFile1 = null;
  }

  void saveanimalimage() async {
    if (pickedFile != null) {
      final directory = await getApplicationDocumentsDirectory();
      final imageDir = Directory('${directory.path}/image');
      if (!imageDir.existsSync()) {
        imageDir.createSync(recursive: true);
      }
      final timestamp = DateTime.now().millisecondsSinceEpoch.toString();
      final fileName =
          '${path.basenameWithoutExtension(pickedFile!.path)}_$timestamp${path.extension(pickedFile!.path)}';
      final savedImage =
          await File(pickedFile!.path).copy('${imageDir.path}/$fileName');
      setState(() {
        _images.add(savedImage);
      });
    } else {
      setState(() {
        _addImageFromAssets();
      });
    }
  }

  Future<void> _deleteImage(File image) async {
    setState(() {
      _images.remove(image);
    });
    await image.delete();
  }

  Future<void> _loadImages() async {
    final directory = await getApplicationDocumentsDirectory();
    final imageDir = Directory('${directory.path}/image');
    if (imageDir.existsSync()) {
      final imageFiles =
          imageDir.listSync().map((item) => File(item.path)).toList();
      // _clearAll();
      setState(() {
        _images = imageFiles;
      });
    }
  }

  Future<void> _getData() async {
    final fetchedData = await DatabaseHelper().getAllData();
    setState(() {
      data = fetchedData;
    });
  }

  void addNewMedicine(
      MediModelClass mediModel, String medicine, String start, String end) {
    mediModel.medicineNames.add(medicine);
    mediModel.startDates.add(start);
    mediModel.endDates.add(end);
    DatabaseHelper().updateTask(mediModel);
    _getData();
  }

  void addNewData() {
    if (nameController.text.isNotEmpty &&
        medicineController.text.isNotEmpty &&
        startDateController.text.isNotEmpty &&
        endDateController.text.isNotEmpty) {
      final newEntry = MediModelClass(
        name: nameController.text.trim(),
        medicineNames: [medicineController.text.trim()],
        startDates: [startDateController.text.trim()],
        endDates: [endDateController.text.trim()],
      );
      DatabaseHelper().insertTask(newEntry);
      _getData();
    }
  }

  void clearController() {
    nameController.clear();
    medicineController.clear();
    startDateController.clear();
    endDateController.clear();
  }

  void showImagePickerOption(BuildContext context, String type) {
    showModalBottomSheet(
        backgroundColor: Colors.blue[100],
        context: context,
        builder: (builder) {
          return Padding(
            padding: const EdgeInsets.all(18.0),
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 4.5,
              child: Row(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        if (type == "animal") {
                          pickImageofanimalFromGallery();
                        } else {
                          pickImageofmedicineFromGallery();
                        }
                      },
                      child: const SizedBox(
                        child: Column(
                          children: [
                            Icon(
                              Icons.image,
                              size: 70,
                            ),
                            Text("Gallery")
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        if (type == "animal") {
                          pickImageofanimalFromCamera();
                        } else {
                          pickImageofmedicineFromCamera();
                        }
                      },
                      child: const SizedBox(
                        child: Column(
                          children: [
                            Icon(
                              Icons.camera_alt,
                              size: 70,
                            ),
                            Text("Camera")
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future _addmedicineimageFromAssets(int cardIndex) async {
    ByteData byteData = await rootBundle.load('asset/image/Designer.png');
    Uint8List imageData = byteData.buffer.asUint8List();

    final directory = await getApplicationDocumentsDirectory();
    final imageDir = Directory('${directory.path}/image');
    if (!imageDir.existsSync()) {
      imageDir.createSync(recursive: true);
    }
    final timestamp = DateTime.now().millisecondsSinceEpoch.toString();
    final fileName = 'image_$timestamp.png';
    final imagePath = '${imageDir.path}/$fileName';

    try {
      final savedImage = await File(imagePath).writeAsBytes(imageData);

      if (savedImage.lengthSync() > 0) {
        setState(() {
          _medicinecards[cardIndex].add(savedImage);
        });
      }
    } catch (e) {
      // print("Error writing image file: $e");
    }
  }

  Future<void> _addImageFromAssets() async {
    // Load image from assets as bytes
    ByteData byteData = await rootBundle.load('asset/image/cow.jpg');
    Uint8List imageData = byteData.buffer.asUint8List();

    final directory = await getApplicationDocumentsDirectory();
    final imageDir = Directory('${directory.path}/image');
    if (!imageDir.existsSync()) {
      imageDir.createSync(recursive: true);
    }
    final timestamp = DateTime.now().millisecondsSinceEpoch.toString();
    final fileName = 'image_$timestamp.png';
    final imagePath = '${imageDir.path}/$fileName';

    try {
      final savedImage = await File(imagePath).writeAsBytes(imageData);

      if (savedImage.lengthSync() > 0) {
        setState(() {
          _images.add(savedImage);
        });
      }
    } catch (e) {
      // print("Error writing image file: $e");
    }
  }

//Gallery image of animal
  Future pickImageofanimalFromGallery() async {
    pickedFile = await _pickeranimal.pickImage(source: ImageSource.gallery);
    Navigator.of(context).pop();
  }

//Camera image of animal
  Future pickImageofanimalFromCamera() async {
    pickedFile = await _pickeranimal.pickImage(source: ImageSource.camera);
    Navigator.of(context).pop();
  }

  //Gallery image of medicine
  Future pickImageofmedicineFromGallery() async {
    pickedFile1 = await _pickermedicine.pickImage(source: ImageSource.gallery);
    Navigator.of(context).pop();
  }

//Camera image of medicine
  Future pickImageofmedicineFromCamera() async {
    pickedFile1 = await _pickermedicine.pickImage(source: ImageSource.camera);
    // final returnImage =
    //     await ImagePicker().pickImage(source: ImageSource.camera);
    // if (returnImage == null) return;
    // setState(() {
    //   selectedIMage = File(returnImage.path);
    //   medicineImage = File(returnImage.path).readAsBytesSync();
    // });
    Navigator.of(context).pop();
  }

  Future<void> _clearAll() async {
    final directory = await getApplicationDocumentsDirectory();
    final imageDir = Directory('${directory.path}/image');
    if (imageDir.existsSync()) {
      imageDir.deleteSync(recursive: true);
    }
    setState(() {
      _images.clear();
      _medicinecards.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: const Color.fromARGB(255, 133, 206, 243),
          items: [
            BottomNavigationBarItem(
              backgroundColor: const Color.fromARGB(255, 133, 206, 243),
              icon: GestureDetector(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (BuildContext context) => const YourWidget()),
                  );
                },
                child: const Icon(
                  Icons.home_outlined,
                  color: Colors.black,
                ),
              ),
              label: 'Home',
            ),
            BottomNavigationBarItem(
                icon: GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const UserListPage()),
                    );
                  },
                  child: const Icon(
                    Icons.account_circle_outlined,
                    color: Colors.black,
                  ),
                ),
                label: "Account"),
          ]),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color.fromARGB(255, 133, 206, 243),
        onPressed: () {
          _medicinecards.add([]);
          showModalBottomSheet(
            isScrollControlled: true,
            isDismissible: true,
            context: context,
            builder: (context) {
              return Padding(
                padding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                  bottom: MediaQuery.of(context).viewInsets.bottom,
                ),
                child: SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(height: 5),
                        const Text(
                          "Add Animal Details",
                          style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(height: 2),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    CircleAvatar(
                                      radius: 50,
                                      backgroundColor: Colors.grey,
                                      child: Stack(
                                        children: [
                                          animalImage$petId != null
                                              ? CircleAvatar(
                                                  radius: 100,
                                                  backgroundImage: FileImage(
                                                      pickedFile as File)
                                                  // MemoryImage(
                                                  //     animalImage$petId!)
                                                  )
                                              : IconButton(
                                                  onPressed: () {
                                                    showImagePickerOption(
                                                        context, "animal");
                                                  },
                                                  icon: const Icon(
                                                    Icons.add_a_photo_outlined,
                                                    size: 40,
                                                  ),
                                                ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(height: 2),
                                    const Text("Add Animal Image"),
                                  ],
                                ),
                              ],
                            ),
                            const SizedBox(height: 5),
                            const Text(
                              "Name",
                              style: TextStyle(fontSize: 16),
                            ),
                            const SizedBox(height: 2),
                            Form(
                              child: Column(
                                children: [
                                  TextFormField(
                                    controller: nameController,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(15),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(15),
                                        borderSide: const BorderSide(
                                          width: 2,
                                          color:
                                              Color.fromARGB(164, 38, 24, 235),
                                        ),
                                      ),
                                      hintText: "Enter Animal Name",
                                      labelText: "Name",
                                    ),
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Please enter the animal name';
                                      }
                                      return null;
                                    },
                                  ),
                                  const SizedBox(height: 2),
                                  const Text(
                                    "Medicine Name",
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  const SizedBox(height: 2),
                                  TextFormField(
                                    controller: medicineController,
                                    maxLines: 2,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(15),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(15),
                                        borderSide: const BorderSide(
                                          width: 2,
                                          color:
                                              Color.fromARGB(164, 38, 24, 235),
                                        ),
                                      ),
                                      hintText: "Enter medicine name",
                                      labelText: "Medicine Name",
                                    ),
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Please enter the medicine name';
                                      }
                                      return null;
                                    },
                                  ),
                                  const SizedBox(height: 5),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            height: 100,
                                            width: 200,
                                            color: Colors.grey,
                                            child: medicineImage != null
                                                ? Image.memory(
                                                    medicineImage!,
                                                    fit: BoxFit.cover,
                                                  )
                                                : IconButton(
                                                    onPressed: () {
                                                      showImagePickerOption(
                                                          context, "medicine");
                                                    },
                                                    icon: const Icon(
                                                      Icons
                                                          .add_a_photo_outlined,
                                                      size: 40,
                                                    ),
                                                  ),
                                          ),
                                          const SizedBox(height: 2),
                                          const Text("Add Medicine Image"),
                                        ],
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    child: const Text(
                                      "Start Date",
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                  const SizedBox(height: 2),
                                  TextFormField(
                                    controller: startDateController,
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      suffixIcon: const Icon(
                                          Icons.calendar_month_outlined),
                                      labelText: "Select Start Date",
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(15),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(15),
                                        borderSide: const BorderSide(
                                          width: 2,
                                          color:
                                              Color.fromARGB(164, 38, 24, 235),
                                        ),
                                      ),
                                      hintText: "Start Date",
                                    ),
                                    onTap: () async {
                                      DateTime? pickeddate =
                                          await showDatePicker(
                                        context: context,
                                        initialDate: DateTime.now(),
                                        firstDate: DateTime(2024),
                                        lastDate: DateTime(3000),
                                      );
                                      if (pickeddate != null) {
                                        String formattedDate =
                                            DateFormat.yMMMd()
                                                .format(pickeddate);
                                        setState(() {
                                          startDateController.text =
                                              formattedDate;
                                        });
                                      }
                                    },
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Please select the start date';
                                      }
                                      return null;
                                    },
                                  ),
                                  const SizedBox(height: 2),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    child: const Text(
                                      "End Date",
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                  const SizedBox(height: 2),
                                  TextFormField(
                                    controller: endDateController,
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      suffixIcon: const Icon(
                                          Icons.calendar_month_outlined),
                                      labelText: "Select End Date",
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(15),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(15),
                                        borderSide: const BorderSide(
                                          width: 2,
                                          color:
                                              Color.fromARGB(164, 38, 24, 235),
                                        ),
                                      ),
                                      hintText: "End Date",
                                    ),
                                    onTap: () async {
                                      DateTime? pickeddate =
                                          await showDatePicker(
                                        context: context,
                                        initialDate: DateTime.now(),
                                        firstDate: DateTime(2024),
                                        lastDate: DateTime(3000),
                                      );
                                      if (pickeddate != null) {
                                        String formattedDate =
                                            DateFormat.yMMMd()
                                                .format(pickeddate);
                                        setState(() {
                                          endDateController.text =
                                              formattedDate;
                                        });
                                      }
                                    },
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Please select the end date';
                                      }
                                      return null;
                                    },
                                  ),
                                  const SizedBox(height: 10),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            IconButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              const AddAlarm()));
                                },
                                icon: const Icon(Icons.add_alarm_outlined,
                                size: 36,
                                )),
                            const Text("add a alram")
                          ],
                        ),
                        const SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Container(
                              height: 40,
                              width: 140,
                              color: Colors.white,
                              child: ElevatedButton(
                                onPressed: () async {
                                  saveanimalimage();
                                  savemedicineimage(data.length);
                                  addNewData();
                                  clearController();
                                  Navigator.of(context).pop();
                                },
                                style: const ButtonStyle(
                                  backgroundColor: WidgetStatePropertyAll(
                                    Color.fromARGB(255, 133, 206, 243),
                                  ),
                                ),
                                child: const Text(
                                  "ADD",
                                  style: TextStyle(
                                      fontSize: 23,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black),
                                ),
                              ),
                            ),
                            Container(
                              height: 40,
                              width: 140,
                              color: Colors.white,
                              child: ElevatedButton(
                                onPressed: () async {
                                  clearController();
                                  Navigator.of(context).pop();
                                },
                                style: const ButtonStyle(
                                  backgroundColor: WidgetStatePropertyAll(
                                    Color.fromARGB(255, 133, 206, 243),
                                  ),
                                ),
                                child: const Text(
                                  "Cancel",
                                  style: TextStyle(
                                      fontSize: 23,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 20),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        },
        child: const Icon(Icons.add),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 15, left: 15, right: 15),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Welcome !!!",
                    style: GoogleFonts.quicksand(
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                      fontSize: 36,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              SizedBox(
                height: 260,
                child: Image.asset("asset/image/p6.jpg"),
              ),
              Consumer<alarmprovider>(builder: (context, alarm, child) {
                return Expanded(
                  child: ListView.builder(
                    itemCount: data.length,
                    itemBuilder: (context, index) {
                      return Slidable(
                        closeOnScroll: true,
                        endActionPane: ActionPane(
                          extentRatio: 0.25,
                          motion: const DrawerMotion(),
                          children: [
                            Expanded(
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  const SizedBox(height: 5),
                                  Container(
                                    height: 40,
                                    width: 40,
                                    decoration: BoxDecoration(
                                      color: const Color.fromARGB(
                                          255, 133, 206, 243),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: IconButton(
                                      onPressed: () {
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              title: const Text(
                                                'Edit Pet',
                                                style: TextStyle(
                                                    color: Colors.blue,
                                                    fontWeight:
                                                        FontWeight.w800),
                                              ),
                                              content: SingleChildScrollView(
                                                child: ListBody(
                                                  children: <Widget>[
                                                    const Text(
                                                      "Pet name :",
                                                      style: TextStyle(
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.w600),
                                                    ),
                                                    TextFormField(
                                                      controller: nameController
                                                        ..text =
                                                            data[index].name,
                                                      decoration:
                                                          const InputDecoration(
                                                              hintText:
                                                                  'Pet Name'),
                                                      validator: (value) {
                                                        if (value == null ||
                                                            value.isEmpty) {
                                                          return 'Please enter the Pet name';
                                                        }
                                                        return null;
                                                      },
                                                    ),
                                                    const SizedBox(
                                                      height: 10,
                                                    ),
                                                    const Text(
                                                      "Medicine Names :",
                                                      style: TextStyle(
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.w600),
                                                    ),
                                                    TextFormField(
                                                      controller:
                                                          medicineController
                                                            ..text = data[index]
                                                                .medicineNames
                                                                .join(' | '),
                                                      decoration:
                                                          const InputDecoration(
                                                              hintText:
                                                                  'Medicine Names'),
                                                      validator: (value) {
                                                        if (value == null ||
                                                            value.isEmpty) {
                                                          return 'Please enter the medicine name';
                                                        }
                                                        return null;
                                                      },
                                                    ),
                                                    const SizedBox(
                                                      height: 10,
                                                    ),
                                                    const Text(
                                                      "Start Dates:",
                                                      style: TextStyle(
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.w600),
                                                    ),
                                                    TextFormField(
                                                      controller:
                                                          startDateController
                                                            ..text = data[index]
                                                                .startDates
                                                                .join(' | '),
                                                      decoration:
                                                          const InputDecoration(
                                                              hintText:
                                                                  'Start Dates'),
                                                      validator: (value) {
                                                        if (value == null ||
                                                            value.isEmpty) {
                                                          return 'Please enter the Start date';
                                                        }
                                                        return null;
                                                      },
                                                    ),
                                                    const SizedBox(
                                                      height: 10,
                                                    ),
                                                    const Text(
                                                      "End Dates :",
                                                      style: TextStyle(
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.w600),
                                                    ),
                                                    TextFormField(
                                                      controller:
                                                          endDateController
                                                            ..text = data[index]
                                                                .endDates
                                                                .join(' | '),
                                                      decoration:
                                                          const InputDecoration(
                                                              hintText:
                                                                  'End Dates'),
                                                      validator: (value) {
                                                        if (value == null ||
                                                            value.isEmpty) {
                                                          return 'Please enter the end date';
                                                        }
                                                        return null;
                                                      },
                                                    ),
                                                    Row(
                                                      children: [
                                                        IconButton(
                                                            onPressed: () {
                                                              Navigator.push(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                      builder:
                                                                          (context) =>
                                                                              const AddAlarm()));
                                                            },
                                                            icon: const Icon(
                                                              Icons
                                                                  .add_alarm_outlined,
                                                              size: 45,
                                                            )),
                                                        const Text(
                                                            "add a alram")
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              actions: <Widget>[
                                                TextButton(
                                                  child: const Text('Cancel'),
                                                  onPressed: () {
                                                    clearController();
                                                    Navigator.of(context).pop();
                                                  },
                                                ),
                                                TextButton(
                                                  child: const Text('Update'),
                                                  onPressed: () {
                                                    data[index].name =
                                                        nameController.text
                                                            .trim();
                                                    data[index].medicineNames =
                                                        medicineController.text
                                                            .trim()
                                                            .split(', ');
                                                    data[index].startDates =
                                                        startDateController.text
                                                            .trim()
                                                            .split(', ');
                                                    data[index].endDates =
                                                        endDateController.text
                                                            .trim()
                                                            .split(', ');
                                                    DatabaseHelper().updateTask(
                                                        data[index]);
                                                    clearController();
                                                    Navigator.of(context).pop();
                                                    _getData();
                                                  },
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                      },
                                      icon: const Icon(
                                        Icons.edit,
                                        color: Colors.black,
                                        size: 20,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    height: 40,
                                    width: 40,
                                    decoration: BoxDecoration(
                                      color: const Color.fromARGB(
                                          255, 133, 206, 243),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: IconButton(
                                      onPressed: () {
                                        showDialog(
                                            context: context,
                                            builder: (context) {
                                              return AlertDialog(
                                                title: const Text("Delete"),
                                                content: const Text(
                                                    "Comform to delete Medicines"),
                                                actions: [
                                                  TextButton(
                                                      onPressed: () async {
                                                        DatabaseHelper()
                                                            .deleteTask(
                                                                data[index]
                                                                    .petId);
                                                        _getData();
                                                        alarm.deleteAlarm(alarm
                                                            .modelist[index]
                                                            .id);
                                                        final image =
                                                            _images[index];
                                                        _deleteImage(image!);
                                                        _deletemedineCard(
                                                            index);
                                                        Navigator.of(context)
                                                            .pop();
                                                      },
                                                      child:
                                                          const Text("Delete")),
                                                  TextButton(
                                                      onPressed: () {
                                                        Navigator.of(context)
                                                            .pop();
                                                      },
                                                      child:
                                                          const Text("Cancel"))
                                                ],
                                              );
                                            });
                                      },
                                      icon: const Icon(
                                        Icons.delete,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 5),
                                ],
                              ),
                            ),
                          ],
                        ),
                        child: Column(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                color: mycolor[index % mycolor.length],
                                borderRadius: BorderRadius.circular(20),
                                boxShadow: const [
                                  BoxShadow(
                                    offset: Offset(8, 8),
                                    color: Color.fromARGB(255, 200, 200, 200),
                                    blurRadius: 2,
                                  ),
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        index < _images.length
                                            ? CircleAvatar(
                                                radius: 40,
                                                backgroundImage:
                                                    FileImage(_images[index]!),
                                              )
                                            : const CircleAvatar(
                                                radius: 40,
                                                backgroundImage: AssetImage(
                                                    "asset/image/cow.jpg"),
                                              ),
                                        const SizedBox(
                                          width: 15,
                                        ),
                                        Text(
                                          "Pet Name: ",
                                          style: GoogleFonts.quicksand(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w800,
                                            fontSize: 22,
                                          ),
                                        ),
                                        Text(
                                          data[index].name,
                                          style: GoogleFonts.quicksand(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w600,
                                            fontSize: 22,
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(height: 5),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        Column(
                                          children: [
                                            Wrap(
                                              direction: Axis.vertical,
                                              spacing: 15,
                                              children: _medicinecards[index]
                                                  .map((image) {
                                                return Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: image != null
                                                      ? Image.file(
                                                          image,
                                                          width: 100,
                                                          height: 60,
                                                          fit: BoxFit.cover,
                                                        )
                                                      : Image.asset(
                                                          "asswt/image/cow.jpg",
                                                          width: 50,
                                                          height: 50,
                                                          fit: BoxFit.cover,
                                                        ),
                                                );
                                              }).toList(),
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          width: 30,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: List.generate(
                                              data[index].medicineNames.length,
                                              (medicineIndex) {
                                            return Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    Text(
                                                      "Medicine: ",
                                                      style:
                                                          GoogleFonts.quicksand(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w800,
                                                        fontSize: 20,
                                                      ),
                                                    ),
                                                    Text(
                                                      data[index].medicineNames[
                                                          medicineIndex],
                                                      style:
                                                          GoogleFonts.quicksand(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 19,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: [
                                                    Text(
                                                      "Start: ",
                                                      style:
                                                          GoogleFonts.quicksand(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w800,
                                                        fontSize: 20,
                                                      ),
                                                    ),
                                                    Text(
                                                      data[index].startDates[
                                                          medicineIndex],
                                                      style:
                                                          GoogleFonts.quicksand(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 19,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: [
                                                    Text(
                                                      "End: ",
                                                      style:
                                                          GoogleFonts.quicksand(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w800,
                                                        fontSize: 20,
                                                      ),
                                                    ),
                                                    Text(
                                                      data[index].endDates[
                                                          medicineIndex],
                                                      style:
                                                          GoogleFonts.quicksand(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 19,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                const SizedBox(height: 5),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        (alarm.modelist.length >
                                                                index)
                                                            ? Text(
                                                                alarm
                                                                    .modelist[
                                                                        index]
                                                                    .dateTime!,
                                                                style: const TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        16,
                                                                    color: Colors
                                                                        .black),
                                                              )
                                                            : const Text(
                                                                "please add alram time"),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .only(
                                                                  left: 8.0),
                                                          child: Text(
                                                              "|${alarm.modelist[index].label}"),
                                                        ),
                                                      ],
                                                    ),
                                                    // CupertinoSwitch(
                                                    //     value: (alarm!.modelist[index]
                                                    //                 .milliseconds! <
                                                    //             DateTime.now()
                                                    //                 .microsecondsSinceEpoch)
                                                    //         ? false
                                                    //         : alarm.modelist[index].check,
                                                    //     onChanged: (v) {
                                                    //       alarm.EditSwitch(index, v);

                                                    //       alarm.CancelNotification(
                                                    //           alarm.modelist[index].id);
                                                    //     }),
                                                  ],
                                                )
                                              ],
                                            );
                                          }),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        IconButton(
                                          onPressed: () {
                                            showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return AlertDialog(
                                                  title: const Text(
                                                      'Add New Medicine'),
                                                  content: Form(
                                                    child:
                                                        SingleChildScrollView(
                                                      child: ListBody(
                                                        children: <Widget>[
                                                          const Text(
                                                            "Medicine Name",
                                                            style: TextStyle(
                                                                fontSize: 16),
                                                          ),
                                                          const SizedBox(
                                                              height: 2),
                                                          TextFormField(
                                                            controller:
                                                                medicineController,
                                                            maxLines: 2,
                                                            decoration:
                                                                InputDecoration(
                                                              border:
                                                                  OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            15),
                                                              ),
                                                              focusedBorder:
                                                                  OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            15),
                                                                borderSide:
                                                                    const BorderSide(
                                                                  width: 2,
                                                                  color: Color
                                                                      .fromARGB(
                                                                          164,
                                                                          38,
                                                                          24,
                                                                          235),
                                                                ),
                                                              ),
                                                              hintText:
                                                                  "Enter medicine name",
                                                              labelText:
                                                                  "Medicine Name",
                                                            ),
                                                            validator: (value) {
                                                              if (value ==
                                                                      null ||
                                                                  value
                                                                      .isEmpty) {
                                                                return 'Please enter the medicine name';
                                                              }
                                                              return null;
                                                            },
                                                          ),
                                                          const SizedBox(
                                                              height: 5),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .center,
                                                                children: [
                                                                  Container(
                                                                    height: 100,
                                                                    width: 200,
                                                                    color: Colors
                                                                        .grey,
                                                                    child: medicineImage !=
                                                                            null
                                                                        ? Image
                                                                            .memory(
                                                                            medicineImage!,
                                                                            fit:
                                                                                BoxFit.cover,
                                                                          )
                                                                        : IconButton(
                                                                            onPressed:
                                                                                () {
                                                                              showImagePickerOption(context, "medicine");
                                                                            },
                                                                            icon:
                                                                                const Icon(
                                                                              Icons.add_a_photo_outlined,
                                                                              size: 40,
                                                                            ),
                                                                          ),
                                                                  ),
                                                                  const SizedBox(
                                                                      height:
                                                                          2),
                                                                  const Text(
                                                                      "Add Medicine Image"),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                          const SizedBox(
                                                              height: 5),
                                                          Container(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            child: const Text(
                                                              "Start Date",
                                                              style: TextStyle(
                                                                  fontSize: 16),
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                              height: 2),
                                                          TextFormField(
                                                            controller:
                                                                startDateController,
                                                            readOnly: true,
                                                            decoration:
                                                                InputDecoration(
                                                              suffixIcon:
                                                                  const Icon(Icons
                                                                      .calendar_month_outlined),
                                                              labelText:
                                                                  "Select Start Date",
                                                              border:
                                                                  OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            15),
                                                              ),
                                                              focusedBorder:
                                                                  OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            15),
                                                                borderSide:
                                                                    const BorderSide(
                                                                  width: 2,
                                                                  color: Color
                                                                      .fromARGB(
                                                                          164,
                                                                          38,
                                                                          24,
                                                                          235),
                                                                ),
                                                              ),
                                                              hintText:
                                                                  "Start Date",
                                                            ),
                                                            onTap: () async {
                                                              DateTime?
                                                                  pickeddate =
                                                                  await showDatePicker(
                                                                context:
                                                                    context,
                                                                initialDate:
                                                                    DateTime
                                                                        .now(),
                                                                firstDate:
                                                                    DateTime(
                                                                        2024),
                                                                lastDate:
                                                                    DateTime(
                                                                        3000),
                                                              );
                                                              if (pickeddate !=
                                                                  null) {
                                                                String
                                                                    formattedDate =
                                                                    DateFormat
                                                                            .yMMMd()
                                                                        .format(
                                                                            pickeddate);
                                                                setState(() {
                                                                  startDateController
                                                                          .text =
                                                                      formattedDate;
                                                                });
                                                              }
                                                            },
                                                            validator: (value) {
                                                              if (value ==
                                                                      null ||
                                                                  value
                                                                      .isEmpty) {
                                                                return 'Please select the start date';
                                                              }
                                                              return null;
                                                            },
                                                          ),
                                                          const SizedBox(
                                                              height: 2),
                                                          Container(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            child: const Text(
                                                              "End Date",
                                                              style: TextStyle(
                                                                  fontSize: 16),
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                              height: 2),
                                                          TextFormField(
                                                            controller:
                                                                endDateController,
                                                            readOnly: true,
                                                            decoration:
                                                                InputDecoration(
                                                              suffixIcon:
                                                                  const Icon(Icons
                                                                      .calendar_month_outlined),
                                                              labelText:
                                                                  "Select End Date",
                                                              border:
                                                                  OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            15),
                                                              ),
                                                              focusedBorder:
                                                                  OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            15),
                                                                borderSide:
                                                                    const BorderSide(
                                                                  width: 2,
                                                                  color: Color
                                                                      .fromARGB(
                                                                          164,
                                                                          38,
                                                                          24,
                                                                          235),
                                                                ),
                                                              ),
                                                              hintText:
                                                                  "End Date",
                                                            ),
                                                            onTap: () async {
                                                              DateTime?
                                                                  pickeddate =
                                                                  await showDatePicker(
                                                                context:
                                                                    context,
                                                                initialDate:
                                                                    DateTime
                                                                        .now(),
                                                                firstDate:
                                                                    DateTime(
                                                                        2024),
                                                                lastDate:
                                                                    DateTime(
                                                                        3000),
                                                              );
                                                              if (pickeddate !=
                                                                  null) {
                                                                String
                                                                    formattedDate =
                                                                    DateFormat
                                                                            .yMMMd()
                                                                        .format(
                                                                            pickeddate);
                                                                setState(() {
                                                                  endDateController
                                                                          .text =
                                                                      formattedDate;
                                                                });
                                                              }
                                                            },
                                                            validator: (value) {
                                                              if (value ==
                                                                      null ||
                                                                  value
                                                                      .isEmpty) {
                                                                return 'Please select the end date';
                                                              }
                                                              return null;
                                                            },
                                                          ),
                                                          const SizedBox(
                                                              height: 10),
                                                              
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  actions: <Widget>[
                                                    TextButton(
                                                      child:
                                                          const Text('Cancel'),
                                                      onPressed: () {
                                                        clearController();
                                                        Navigator.of(context)
                                                            .pop();
                                                      },
                                                    ),
                                                    TextButton(
                                                      child: const Text('Add'),
                                                      onPressed: () {
                                                        setState(() {
                                                          savemedicineimage(
                                                              index);
                                                          addNewMedicine(
                                                            data[index],
                                                            medicineController
                                                                .text
                                                                .trim(),
                                                            startDateController
                                                                .text
                                                                .trim(),
                                                            endDateController
                                                                .text
                                                                .trim(),
                                                          );
                                                          clearController();
                                                          Navigator.of(context)
                                                              .pop();
                                                        });
                                                      },
                                                    ),
                                                  ],
                                                );
                                              },
                                            );
                                          },
                                          icon: const Icon(
                                            Icons.add_circle_outline,
                                            color: Colors.black,
                                          ),
                                        ),
                                        const Text("Add New medicine"),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(height: 18),
                          ],
                        ),
                      );
                    },
                  ),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}
