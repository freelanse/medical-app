class MediModelClass {
  int? petId;
  String name;
  List<String> medicineNames;
  List<String> startDates;
  List<String> endDates;

  MediModelClass({
    this.petId,
    required this.name,
    required this.medicineNames,
    required this.startDates,
    required this.endDates,
  });

  // Convert a MediModelClass object into a Map object
  Map<String, dynamic> toMap() {
    return {
      'petId': petId,
      'name': name,
      'medicineNames': medicineNames.join(','),
      'startDates': startDates.join(','),
      'endDates': endDates.join(','),
    };
  }

  // Extract a MediModelClass object from a Map object
  factory MediModelClass.fromMap(Map<String, dynamic> map) {
    return MediModelClass(
      petId: map['petId'],
      name: map['name'],
      medicineNames: map['medicineNames'].split(','),
      startDates: map['startDates'].split(','),
      endDates: map['endDates'].split(','),
    );
  }
}
