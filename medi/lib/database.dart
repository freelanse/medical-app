import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as path;
import 'model_anm.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper._internal();
  factory DatabaseHelper() => _instance;
  DatabaseHelper._internal();

  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDatabase();
    return _database!;
  }

  Future<Database> _initDatabase() async {
    return await openDatabase(
      path.join(await getDatabasesPath(), 'medi_database1.db'),
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE medi(petId INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, medicineNames TEXT, startDates TEXT, endDates TEXT)',
        );
      },
      version: 1,
    );
  }

  Future<void> insertTask(MediModelClass mediModelClass) async {
    final db = await database;
    await db.insert(
      'medi',
      mediModelClass.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<MediModelClass>> getAllData() async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('medi');
    return List.generate(maps.length, (i) {
      return MediModelClass.fromMap(maps[i]);
    });
  }

  Future<void> updateTask(MediModelClass mediModelClass) async {
    final db = await database;
    await db.update(
      'medi',
      mediModelClass.toMap(),
      where: 'petId = ?',
      whereArgs: [mediModelClass.petId],
    );
  }

  Future<void> deleteTask(int? petId) async {
    final db = await database;
    await db.delete(
      'medi',
      where: 'petId = ?',
      whereArgs: [petId],
    );
  }
}
