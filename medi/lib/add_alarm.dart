import 'dart:math';

import 'package:medi/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class AddAlarm extends StatefulWidget {
  const AddAlarm({super.key});

  @override
  State<AddAlarm> createState() => _AddAlaramState();
}

class _AddAlaramState extends State<AddAlarm> {
  late TextEditingController controller;
  

  String? dateTime;
  bool repeat = false;

  DateTime? notificationtime;

  String? name = "none";
  int? Milliseconds;

  @override
  void initState() {
    controller = TextEditingController();
    context.read<alarmprovider>().GetData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 133, 206, 243),
        actions: const [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.check),
          )
        ],
        automaticallyImplyLeading: true,
        title: const Text(
          'Add Alarm',
          style: TextStyle(fontWeight: FontWeight.bold,fontSize: 26),
        ),
        centerTitle: true,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.3,
            width: MediaQuery.of(context).size.width,
            child: Center(
                child: CupertinoDatePicker(
              showDayOfWeek: true,
              minimumDate: DateTime.now(),
              dateOrder: DatePickerDateOrder.dmy,
              onDateTimeChanged: (va) {
                dateTime = DateFormat().add_jms().format(va);

                Milliseconds = va.microsecondsSinceEpoch;

                notificationtime = va;

              },
            )),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: CupertinoTextField(
                      placeholder: "Add Label",
                      controller: controller,
                    )),
                    
              ],
            ),
          ),
          Row(
            children: [
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(" Repeat daily",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
              ),
              CupertinoSwitch(
                value: repeat,
                onChanged: (bool value) {
                  repeat = value;

                  if (repeat == false) {
                    name = "none";
                  } else {
                    name = "Everyday";
                  }

                  setState(() {});
                },
              ),
            ],
          ),

          const SizedBox(
            height: 20,
          ),
          ElevatedButton(

              onPressed: () {
                Random random = new Random();
                int randomNumber = random.nextInt(100);

                context.read<alarmprovider>().SetAlaram(controller.text,
                    dateTime!, true, name!, randomNumber, Milliseconds!);
                context.read<alarmprovider>().SetData();

                context
                    .read<alarmprovider>()
                    .SecduleNotification(notificationtime!, randomNumber);

                Navigator.pop(context);
              },
               style: ButtonStyle(backgroundColor:  WidgetStateProperty.all(const Color.fromARGB(255, 133, 206, 243))),
              child: const Text("Set Alaram",style: TextStyle(fontSize: 18,color: Colors.black),),
              ),
        ],
      ),
    );
  }
}
