import 'dart:async';
import 'package:medi/accountPage.dart';
import 'package:medi/add_alarm.dart';
import 'package:medi/homep.dart';
import 'package:medi/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool value = false;

  @override
  void initState() {
    context.read<alarmprovider>().Inituilize(context);
    Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {});
    });

    super.initState();
    context.read<alarmprovider>().GetData();
  }


    List<Color> mycolor = [
    Colors.pink.shade100,
    Colors.blue.shade100,
    Colors.purple.shade100,
    Colors.green.shade100,
    Colors.yellow.shade100
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
                   bottomNavigationBar: BottomNavigationBar(backgroundColor: const Color.fromARGB(255, 133, 206, 243),
              items:  [
        BottomNavigationBarItem(
          backgroundColor: const Color.fromARGB(255, 133, 206, 243),
          icon: GestureDetector(
            onTap: (){ Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => const YourWidget()
                ),
              );},
            child: const Icon(
              Icons.home_outlined,
              color: Colors.black,
            ),
          ),
          label: 'Home',
        ),
        
        BottomNavigationBarItem(
            icon:GestureDetector(
              onTap: () {
                 Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => const MyApp()
                ),
              );
              },
              child: const Icon(
                Icons.alarm,
                color: Colors.black,
              ),
            ),
            label: "Alarm"),
        BottomNavigationBarItem(
            icon: GestureDetector(
              onTap: () {
                 Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => const UserListPage()
                ),
              );
              },
              child: const Icon(
                Icons.account_circle_outlined,
                color: Colors.black,
              ),
            ),
            label: "Account"),
      ]),

     floatingActionButton: FloatingActionButton(onPressed: (){
       Navigator.push(context,
                     MaterialPageRoute(builder: (context) => const AddAlarm()));
     },
     backgroundColor: const Color.fromARGB(255, 133, 206, 243),child:  const Icon(Icons.add,color: Colors.black,),),
      backgroundColor: Colors.white,

      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 133, 206, 243),
        actions: const [
          Padding(
            padding: EdgeInsets.all(8.0),
          )
        ],
        title: const Text(
          'Add Alarm',
          style: TextStyle(color: Colors.black,fontSize: 26,fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          Container(
            decoration: const BoxDecoration(
                color: Color.fromARGB(255, 133, 206, 243),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30))),
            height: MediaQuery.of(context).size.height * 0.1,
            child: Center(
                child: Text(
              DateFormat.yMEd().add_jms().format(
                    DateTime.now(),
                  ),
              style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 19,
                  color: Colors.black),
            )),
          ),
          Consumer<alarmprovider>(builder: (context, alarm, child) {
            return SizedBox(
              height: MediaQuery.of(context).size.height * 0.7,
              child: ListView.builder(
                  itemCount: alarm.modelist.length,
                  itemBuilder: (BuildContext, index) {
                    return Padding(
                        padding: const EdgeInsets.all(10),
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.150,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          alarm.modelist[index].dateTime!,
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.black),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 8.0),
                                          child: Text("|${alarm.modelist[index].label}"),
                                        ),
                                      ],
                                    ),
                                    CupertinoSwitch(
                                        value: (alarm.modelist[index]
                                                    .milliseconds! <
                                                DateTime.now()
                                                    .microsecondsSinceEpoch)
                                            ? false
                                            : alarm.modelist[index].check,
                                        onChanged: (v) {
                                          alarm.EditSwitch(index, v);

                                          alarm.CancelNotification(
                                              alarm.modelist[index].id);
                                        }),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(alarm.modelist[index].when!),
                                    IconButton(
                                        onPressed: () {
                                          alarm.deleteAlarm(
                                              alarm.modelist[index].id);
                                        },
                                        icon: const Icon(
                                          Icons.delete,
                                          color: Colors.black,
                                        ))
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ));
                  }),
            );
          }),
         
        ],
      ),
    );
  }
}